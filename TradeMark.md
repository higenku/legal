# TradeMark
Last updated: 2021-12-07 UTC <br>
Version: 1.0.0

@lmtr0 (lmtr0@protonmail.ch) owns the "Higenku" name and its logos copyright.<br>
If you want to use the name, brands or logo please follow the guide bellow.<br>
Third-party logos may be subject to third-party copyrights and trademarks.

## Brand
Higenku is a copytigh protected name. Use of the name is possible under specific circumstances or with written approval from the Higenku founder (lmtr0). Inproper use of the Higenku name or assets may result in preventative action being taken to protect the brand image. 

### What you can do
- Mention us, write about us, link to us
- Provide services or products related to Higenku by postfixing it with "for Higenku"

### What you can't do
- Using "higenku" as a product/service title, account or company name.


## Logos
The Higenku logos are protected under a copyright. Using the logo, or derivatives, as your own, for your own services or company is not allowed. This includes the logo with and without the name.

### What you can do
- Use the original in the same form and color
- Use it to link to the Higenku Project
- Modify the logo, to improve it, criticize it and make comedy with it.

### What you cannot do
- Publish, distribute, sublicense, and/or sell as your own


## Domains
Registering a domain name containing the name "Higenku" is possible without written agreement by the Founder of the Higenku Project (lmtr0) under these circumstances.

### What you can do
- Non-commercial domains that are - ignoring the tld - not an exact match, like: mycoolstuffmadewithHIGENKUproducts.org.
- Exact matches (higenku.uk) and commercial domain names are only allowed with a written agreement from the Higenku Project (lmtr0).

### What you cannot do
- Domains that are exact matches, like: higenku.co.uk, higenku.blog.
- Domains that form the name "higenku" by combining second- and third levels, like: hi.gen.ku.org and higen.ku.co.
- Any for-profit/commercial domain that contains higenku without written agreement by the the Higenku founder (lmtr0).
- this is only to prevent scams

## Contact Us
please refer to the [Contact Us Clause](./TermsOfService/#contact-us) in the [Terms of Service](./TermsOfService) Document.
