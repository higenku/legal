Before raising an issue to the our issue tracker, please read through our contributing guide:

* https://higenku.org/legal/contributing/

If you are experiencing an issue when using this issue tracker, your first port of call should be the Community Chat. Your issue may have already been reported there by another user. Please check:

* https://higenku.org/support/

If you feel that your issue can be categorized as a reproducible bug or a feature proposal, please use one of the issue templates provided and include as much information as possible.

Thank you for helping to make Higenku a better software.
