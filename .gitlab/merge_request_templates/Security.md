## Related issues

## Developer checklist

- [ ] **On "Related issues" section, write down the [GitLab Security] issue it belongs to (i.e. `Related to <issue_id>`).**
- [ ] Merge request targets `dev`, or a versioned stable branch (`X-Y-stable`).
- [ ] Title of this merge request is the same as for all backports.
- [ ] A [CHANGELOG entry] has been included, with `Changelog` trailer set to `Security`.
- [ ] For a backport MR targeting a versioned stable branch (`X-Y-stable`).
  - [ ] Ensure it's approved by a maintainer.

## Maintainer checklist
- [ ] Correct milestone is applied and the title is matching across all backports.
- [ ] Assigned (_not_ as reviewer) to `@gitlab-release-tools-bot` with passing CI pipelines.

/label ~"type::Security"
/assign me
/confidential
