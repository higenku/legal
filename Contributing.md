
# Higenku Contributing Guides
Make sure you read this carefully before doing anything and that you have agreed to the [Contributor License Agreement (CLA)](https://higenku.org/legal/cl)

# Introduction
Firstly, thank you for taking the time to read this long document on how to contribute to our projects. We are very glad :)
So you want to contribute to our project? Thank you, it's because of awesome people like you that our project exists. So please, after you make your contribution, open a PR in [this repo](https://github.com/Higenku/Atributions) to add your name to our [contributor portal](https://higenku.org/acknowledgments).

Following these guidelines helps to communicate that you respect the time of the developers managing and developing this open source project. In return, they should reciprocate that respect in addressing your issue, assessing changes, and helping you finalize your Merge Requests.

## General Considerations
The Higenku Project is an open source project and we love to receive contributions from our community — you! There are many ways to contribute, from writing tutorials or blog posts, improving the documentation, submitting bug reports and feature requests or writing code which can be incorporated into our projects.

Please, **don't use the issue tracker** for [support questions]. Check the our [Forum](https://community.higenku.org), [discord and matrix](https://higenku.org/contact) for this kinds of issues.

# Ground Rules
Ensure you follow our [code of conduct](https://higenku.org/legal/coc) 

Responsibilities
* Ensure cross-platform compatibility for every change that's accepted. Windows, Mac and Linux.
* Ensure that code that goes into respects the coding conventions of the project you are contributing to. The coding conventions are specified in the Supplementary Guides of each project.
* Create issues for any major changes and enhancements that you wish to make. Discuss things transparently and get community feedback.
* Don't add random useless code that doesn't make any difference in the repository.
* Keep feature versions as small as possible, preferably one new feature per version.
* Be welcoming to newcomers and encourage diverse new contributors from all backgrounds.


# Getting started
make sure you agreed to out [Contributor License (CL)](https://higenku.org/legal/cl) and the license of the project you are contributing. The project may also have supplementary guides for you to follow. Additionally, you are expected to follow our code of conduct.

we use gitlab to manage the development of features, bug reports, and overall tracking of the development of the project. So, there is a lot of planning work done in gitlab. You can start by looking into it. 

For something that is bigger than a one or two line fix:
1. Create your own fork of the code
2. Do the changes in your fork
3. If you like the change and think the project could use it:
    * Be sure you have followed the code style for the project.
    * Send a Merge Request indicating that you have a CLA on file.

Small contributions such as fixing spelling errors, where the content is small enough to not be considered intellectual property, can be submitted by a contributor as a patch, without a CLA.

As a rule of thumb, changes are obvious fixes if they do not introduce any new functionality or creative thinking. As long as the change does not affect functionality, some likely examples include the following:
* Spelling / grammar fixes
* Typo correction, white space and formatting changes
* Comment clean up
* Bug fixes that change default return values or error codes stored in constants
* Adding logging messages or debugging output
* Changes to ‘metadata’ files like Gemfile, .gitignore, build scripts, etc.
* Moving source files from one directory or package to another

# Your First Contribution
If you are new here, you can help:
- review a Merge Request
- fix an Issue
- update the documentation
- make a website
- write a tutorial
- write a testapp
    - look more in [sdks/testapps](https://gitlab.com/higenku/sdks/testapps)
- write code to develop the project
    - we have projects in Sveltekit such as [apps/site](https://gitlab.com/higenku/apps/site) and [apps/docs](https://gitlab.com/higenku/apps/site)
    - we also have rust projects such as [apps/account](https://gitlab.com/higenku/apps/account), [apps/store](https://gitlab.com/higenku/apps/site), [apps/client](https://gitlab.com/higenku/apps/site) and [tools/packer](https://gitlab.com/higenku/tools/packer)
    - you can also look into the code to see if you found a format issue :)

# How to report a bug
If you find a **security vulnerability**, do NOT open an issue. Email support@higenku.org instead. Add **[SECURITY BUG]** to the subject please :)

In order to determine whether you are dealing with a security issue, ask yourself these two questions:
* Can I access something that's not mine, or something I shouldn't have access to?
* Can I disable something for other people?

If the answer to either of those two questions are "yes", then you're probably dealing with a security issue. Note that even if you answer "no" to both questions, you may still be dealing with a security issue, so if you're unsure, just email us at security@travis-ci.org.

Other issues can be submitted as issues in the gitlab of the project. The supplementary guides may contain more guides for issues but ussualy it's:
- Please provide information about the environment you were using. Browser, Browser version, System, version of the program or webapp...
- Please describe the issue in detail specifying important information. **DO NOT** include sensible information in the issues, use support@higenku.org for that because issues there are private.
- What did you do?
- What did you expect to see?
- What did you see instead?
- General questions should go to our [Matrix / Discord](https://higenku.org/contact)

# How to suggest a feature or enhancement
If you find yourself wishing for a feature that doesn't exist in one of our projects, you are probably not alone. There are bound to be others out there with similar needs. Please open an issue on our issues the Gitlab of the project you are using which describes the following:
- the feature you would like to see
- why you need it (If possible)
- and how it should work.
- Please provide an implementation spec of it (if you have already though of it)

# Code review process
The core team looks into the Merge Request. Evaluate it, gives feedback and if CI is present and complete, we merge it. However if the feature is Huge, we will first discuss it with the community in our Monthly meeting.

The Monthly meetings are announced in our [Matrix / Discord](https://higenku.org/contact) update channels. However, the meeting itself may be hosted in discord or jitsi. We then discuss major changes in the project, features to come, where our main focus is and discuss Huge Merge request ang get feedback in it.

After feedback has been given (both in the Monthly meeting and in gitlab) we expect responses within 4 weeks. After 4 weeks we may close the Merge Request if it isn't showing any activity.

# Community
Be aware that there is other community channels that we use. Look at [this page](https://higenku.org/contact) for more information.

# Commit message conventions
We don't have specific lable conventions, remember that a good commit message makes the day of the reviewers :). However, we do have some **Suggested Labels**:
## Examples
- Used for when a bug is corrected:
    - Corrected bug with [...] Closes #[Issue]
    - Closes #[Issue_number]
    - Closes #[Issue_number]
    - Corrected CI script
    - Corrected Cloudflare build
- Used when implementing a Feature:
    - Started Implementing [...] from #[issue]
    - Implementing [...] from #[issue]
- Used when you improved Performance of code:
    - Made the [...] faster
    - Improvement on [...] speed
- Used when Cleaning stuff:
    - Cleaning [...]
    - Visual changes
    - Refactoring the code
- Used for Documentation:
    - Wrote doc on [...]
    - Finished doc on [...]
    - Started doc on [...]
    - Started doc on [...] needs more work
- Used when review code:
    - Reviewed code [...]
    - Changing typo [...]
- Used for general things: 
    - Worked on [...]
    - Worked with [...]
- Used for testing:
    - Corrected Tests
    - Testing [...]

## Blockers Examples
- Needs Bug Verification
- Needs Reproduction
- Has Reproduction
- Ready for PR
- PR Pending
- Needs Code Review
- Needs Submitter Response
- Needs Team Discussion
- Needs implementation of api [...] from [...]

# Submit Feedback
The best way to send feedback is to our community category [feedback](https://community.higenku.org/t/feedback), but you can also use our [Matrix / Discord](https://higenku.org/contact) 

If you are proposing a feature:

Explain in detail how it would work.
Keep the scope as narrow as possible, to make it easier to implement.
Remember that this is a volunteer-driven project, and that contributions are welcome :)

# Merge Requets
After forking the project and worked on the change you made. You will have to open a Merge Request (also called a MR, PR or Pull Request), which needs to include the following:
- A statement confirming that you agreed to our CLA
- A clear description of what the change does
- Why you implementation
- Considerations (For example: doens't work on mac because...)
- Comments about the change (For example: I think the webview envent loop needs more work before it becomes stable)

Then, you just need to our feedback, discuss and work on the changes we suggest. Then we will consider merging you MR. Finally, after the merge, Add your indentification to our [attributions repository](https://github.com/Higenku/Atributions/blob/master/CONTRIBUTING.md) and then, after a day or two, apreciate your name in our [contributor portal](https://higenku.org/acknowledgments)


# Final
Thanks for your attention!
Higenku Developers and [Contributors](https://higenku.org/acknowledgments)!

