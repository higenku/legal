# Cookie Policy 
Last updated: 2021-12-07 UTC <br>
Version: 1.0.0

## 1.1 Why do we use cookies?  
Here at our organization, we like the concept of privacy, so we decided to store as little data as possible from our users. The only data we store is for verification and the data from our third-party service providers (this includes Cloudflare for DNS management). Our cookies make it possible for you to:  

### 1.1.1 Preference  
This includes, for example, the theme you are using and the language you preferer.  

### 1.1.2 Authentication  
This is for us to identify if you are you and not someone else trying to be you  

### 1.1.3 Cloudflare  
These are set by Cloudflare and you can learn more here  

### 1.1.4 Gitlab 
We use Gitlab to host our code and therefore some static sites like the documentation, set cookies for authentication and some other stuff that you can learn more about here 

### 1.2 Do Not Track Disclosure  
At this time, we do not respond to browser “Do Not Track” signals, because we do not track our users.  

### 1.3 Who sets cookies in our platform  
Our cookies are most of the time placed by our platforms and sometimes by our service provides. Cloudflare for example, place cookies for analytics, detect bots, and identifies some stuff that we don’t use.  

### 1.4 Right of change  
We grant ourselves the right to change this document at any time and without prior notice. However, we will give you notice after we change it with what we changed, why we changed it and how it will work from that date onwards.

### Contact Us
please refer to the [Contact Us Clause](/legal/tos#contact-us) in the [Terms of Service](/legal/tos) Document.
