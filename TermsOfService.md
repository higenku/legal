# ATTENTION THIS DOCUMENT IS WORK IN PROGRESS AND IS NOT VALID UNTIL THIS MESSAGE IS NO LONGER HERE
# Terms of Service
Last updated: 2021-12-07 UTC <br>
Version: 1.0.0-alpha

## Contact Us 

### Direct Communication by Email
**Legal** email line, for legal related information, Copyright claims, etc. Please use [hello@higenku.org](mailto:hello@higenku.org), and in your subject start with `[Legal]: `. <br>
**Payment** email line, for doughts related to payment, cancelation of service, etc. Please use [help@higenku.org](mailto:help@higenku.org), and in your subject start with `[Payment]: `.<br>
**Information**: email line, for general doughts and questions, etc. Please use [help@higenku.org](mailto:help@higenku.org), and in your subject start with `[Information]: `.<br>
**User**: email line, for User Specific doughts, questions, alteration of data, etc. Please use [help@higenku.org](mailto:help@higenku.org), and in your subject start with `[Information]: `.

### Other

Please send your feedback, comments in a discussion in our [Community](https://discuss.higenku.org/c/feedback).<br>
For Abuse, Private issues, or security reports, please follow with the [Countributing](./Contributing) document.<br>
For general communication please use [hello@higenku.org](mailto:hello@higenku.org), [discord]() or our [Community](https://discuss.higenku.org/c/general)

## Definitions 
- **Higenku Project Authors**: [lmtr0](https://gitlab.com/lmtr0) and [Gustavo Peredo](https://gitlab.com/users/GustavoPeredo) (contact at \<hello@higenku.org\>)
- **Higenku Project, Higenku Project Administration, Project, we, our, us**: The Project behind these terms and the copyright holder for the applications with the Copyright notice including, but not limited to, Made by Higenku, made by Higenku Devs, made by the Higenku Project. 
- **Terms of Service, Terms**: This document 
- **Content**: Refers to everything that a user can create, post, upload, and or delete in our site. Not Including Accounts
- **Store**: The higenku application present in [store.higenku.org](https://store.higenku.org) which aims to provide a distributed store for cross-platforms applications.
- **Store Apps**: The app present in [store.higenku.org](https://store.higenku.org)

## Introduction 

Welcome to the Higenku Project  <br>
These Terms of Service govern your use of our all websites located under *.higenku.org (including, but not limited to, accounts.higenku.org, store.higenku.org, docs.higenku.org, community.higenku.org and higenku.org) operated by Higenku Project Administration. <br>
Our Privacy Policy also governs your use of our Service and explains how we collect, safeguard and disclose information that results from your use of our web pages. <br>
Your agreement with us includes these Terms and our Privacy Policy (“Agreements”). You acknowledge that you have read and understood Agreements, and agree to be bound of them. <br>
If you do not agree with (or cannot comply with) Agreements, then you may not use the Service, but please let us know why by emailing us.<br>
These Terms apply to all visitors, users and others who wish to access or use Service. 

<!-- 
Todo: Needs to be rewritten because we will not offer for ourselves and instead on behave of others in the future

## Purchases 

If you wish to purchase any product or service made available through Service (“Purchase”), you may be asked to supply certain information relevant to your Purchase including but not limited to, your credit or debit card number, the expiration date of your card, your billing address, and your shipping information. <br>
You represent and warrant that: (i) you have the legal right to use any card(s) or other payment method(s) in connection with any Purchase; and that (ii) the information you supply to us is true, correct and complete. <br> 
We may employ the use of third-party services for the purpose of facilitating payment and the completion of Purchases. By submitting your information, you grant us the right to provide the information to these third parties subject to our Privacy Policy. <br> 
We reserve the right to refuse or cancel your order at any time for reasons including but not limited to: product or service availability, errors in the description or price of the product or service, error in your order or other reasons, staff abuse, impoliteness with our developers, contact spamming and break of these Terms. <br>
We reserve the right to refuse or cancel your order if fraud or an unauthorized or illegal transaction is suspected.<br> 
“Other Orders” mainly orders placed from our payment system in our store grant the right of “to the developer and or Project behind the app. <br>

## Contests, Sweepstakes and Promotions 

Any contests, sweepstakes or other promotions (collectively, “Promotions”) made available through Service may be governed by rules that are separate from these Terms of Service. If you participate in any Promotions, please review the applicable rules as well as our Privacy Policy. If the rules for a Promotion conflict with these Terms of Service, Promotion rules will apply. <br>
These also apply to Promotions on apps that are outside the power of Higenku Project. Developers are free to make Promotions on their apps and these Terms do not apply to those cases. <br>

## Subscriptions 

Some parts of Service are billed on a subscription basis ("Subscription(s)"). You will be billed in advance on a recurring and periodic basis ("Billing Cycle"). Billing cycles will be set depending on the type of subscription plan you select when purchasing a Subscription. <br>
At the end of each Billing Cycle, your Subscription will automatically renew under the exact same conditions unless you cancel it or Higenku Project cancels it. You may cancel your Subscription renewal either through your online account management page or by contacting Payment Email line. <br>
A valid payment method is required to process the payment for your subscription. You shall provide Higenku Project with accurate and complete billing information that may include but not limited to full name, address, state, postal or zip code, telephone number, and a valid payment method information. By submitting such payment information, you automatically authorize Higenku Project to charge all Subscription fees incurred through your account to any such payment instruments. <br>
Should automatic billing fail to occur for any reason, Higenku Project reserves the right to terminate your access to the Service with immediate effect, this also includes subscriptions from the third party apps from the store. <br> 
In case of errors in our system including, but not limited to, charge occurring after the service was cancelled, you reserve the right to open a ticket on our forum and claim for refund. But we also reserve the right to refuse your refund if we evaluate your actions as suspicious or fraud. <br>

## Free Trial 

We may, at its sole discretion, offer a Subscription with a free trial for a limited period of time ("Free Trial"). <br>
You may be required to enter your billing information in order to sign up for Free Trial. <br>
If you do enter your billing information when signing up for Free Trial, you will not be charged by Higenku Project until Free Trial has expired. On the last day of Free Trial period, unless you cancelled your Subscription, you will be automatically charged the applicable Subscription fees for the type of Subscription you have selected. <br>
At any time and without notice, we reserves the right to (i) modify Terms of Service of Free Trial offer, or (ii) cancel such Free Trial offer. <br>

## Fee Changes 

Higenku Project, in its sole discretion and at any time, may modify Subscription fees for the Subscriptions. Any Subscription fee change will become effective at the end of the then-current Billing Cycle. <br>
Higenku Project will provide you with a reasonable prior notice of any change in Subscription fees to give you an opportunity to terminate your Subscription before such change becomes effective. <br>
Your continued use of Service after Subscription fee change comes into effect constitutes your agreement to pay the modified Subscription fee amount. <br>

## Refunds 

We issue refunds for Contracts withindays of the original purchase of the Contract.  <br>
When refunds are issued, we reserve the right to terminate your access to the Service or App that you claim refund to. The Data, after deleted, CAN NOT be restored and we reserve the right to delete it after the refund is processed. The app may ask you to download the account backup in order to restore the app if you decide to come back at any time. <br> 
-->

## Content 

Some of our products, allow you to create Content. You are responsible for Content that you post on or through Service, including its legality, reliability, and appropriateness. <br>
By posting Content on or through Service, You represent and warrant that: (i) Content is yours (you own it) and/or you have the right to use it and the right to grant us the rights and license as provided in these Terms, and (ii) that the posting of your Content on or through Service does not violate the privacy rights, publicity rights, copyrights, contract rights or any other rights of any person or entity. We reserve the right to terminate the account of anyone found to be infringing on a copyright. <br> 
You retain any and all of your rights to any Content you submit, post or display on or through Service and you are responsible for protecting those rights. We take no responsibility and assume no liability for Content you or any third party posts on or through Service. However, by posting Content using Service you grant us the right and license to distribute such Content on and through Service. You agree that this license includes the right for us to make your Content available to other users of Service, who may also use your Content subject to these Terms, but also granting copyright notice and rights to a license, including but not limited to, [Creative Commons license](https://creativecommons.org/licenses/), [Gnu Licenses](https://www.gnu.org/licenses/) and [Open Source Licenses](https://opensource.org/licenses). <br>
Higenku Project has the right but not the obligation to monitor and edit all Content provided by users, not including the applications on our Store. When edit, our editions are obligated to follow the license following the content, if no license is provided, the  [CC BY v4.0](https://creativecommons.org/licenses/by/4.0/) is assumed to be the provided license. <br>
Applications distributed by the Store app will also follow the **Applications Distributed on the Store** Clause, in case of conflicting subclauses, the **Applications Distributed on the Store** Clause is overriding the clause.<br>

## Prohibited Uses 

### You may use Service only for lawful purposes and in accordance with Terms. You agree not to use Service: 

- In any way that violates any applicable national or international law or regulation. 
- For the purpose of exploiting, harming, or attempting to exploit or harm minors in any way by exposing them to inappropriate content or otherwise. 
- To transmit, or procure the sending of, any advertising or promotional material, including but not limited to “junk mail”, “chain letter,” “spam,” or any other similar solicitation. 
- To impersonate or attempt to impersonate Project, a Project employee, another user, or any other person or entity. 
- In any way that infringes upon the rights of others, or in any way is illegal, threatening, fraudulent, or harmful, or in connection with any unlawful, illegal, fraudulent, or harmful purpose or activity. 
- To engage in any other conduct that restricts or inhibits anyone’s use or enjoyment of Service, or which, as determined by us, may harm or offend Project or users of Service or expose them to liability. 
- To engade and to promote malicious purpose and discrimatory groups.

### Additionally, you agree not to: 

- Use Service in any manner that could disable, overburden, damage, or impair Service or interfere with any other party’s use of Service, including their ability to engage in real time activities through Service. 
- Use any robot, spider, or other automatic device, process, or means to access Service for any purpose, including monitoring or copying any of the material on Service. 
- Use any manual process to monitor or copy any of the material on Service or for any other unauthorized purpose without our prior written consent. 
- Use any device, software, or routine that interferes with the proper working of Service. 
- Introduce any viruses, trojan horses, worms, logic bombs, or other material which is malicious or technologically harmful. 
- Attempt to gain unauthorized access to, interfere with, damage, or disrupt any parts of Service, the server on which Service is stored, or any server, computer, or database connected to Service. 
- Attack Service via a denial-of-service attack or a distributed denial-of-service attack. 
- Take any action that may damage or falsify Project rating. 
- Otherwise attempt to interfere with the proper working of Service. 

## Analytics 

We may use third-party Service Providers to monitor and analyze the use of our Service. including, but not limited to cloudflare. <br>
We assure you that your privacy is protected and the analytics is totally anonimos and does not track anything specific. <br>
Here are some of the information our current analytics collect:
- Region of the request: Which country the request is comming from
- Unique Requests: How many unique request we had.


## No Use By Children

Service is intended only for access and use by individuals at least fourteen (14) years old. By accessing or using Service, you warrant and represent that you are at least fourteen (14) years of age and with the full authority, right, and capacity to enter into this agreement and abide by all of the terms and conditions of Terms. If you are not at least fourteen (14) years old, you are prohibited from both the access and usage of Service. When discoverd that this clause is violated, your Account will be terminated and all access to our service denied.

## Accounts 
When you create an account with us, you guarantee that you are above the age of **14**, and that the information you provide us is accurate, complete, and current at all times. Inaccurate, incomplete, or obsolete information may result in the immediate termination of your account on Service.<br>
You are responsible for maintaining the confidentiality of your account and password, including but not limited to the restriction of access to your computer and/or account. You agree to accept responsibility for any and all activities or actions that occur under your account and/or password, whether your password is with our Service or a third-party service. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account.<br>
You may not use as a username the name of another person or entity or that is not lawfully available for use, a name or trademark that is subject to any rights of another person or entity other than you, without appropriate authorization.<br>
If for any reason you as an individual or entity found your username in others name with the intention of looking like you in order to distribute, post or comment in our platform. You may [Contact Us](#contact-us)<br>
We reserve the right to refuse service, terminate accounts, remove or edit content, or cancel orders in our sole discretion.<br>
**You Agree to the responsability of creating and using long, secure and unique passwords. and to not use super simple passwords.**<br>

## Intellectual Property 

Service and its original content (excluding Content provided by users and third party libraries and or software used by us, including but not limited to https://docsearch.higenku.org), features and functionality are and will remain the exclusive property of Higenku Project and its licensors. Service is protected by copyright, trademark, and other laws of and foreign countries. Our trademarks may not be used in connection with any product or service without the prior written consent of Higenku Project Authors.

## Copyright Policy 

We respect the intellectual property rights of others. It is our policy to respond to any claim that Content posted on Service infringes on the copyright or other intellectual property rights (“Infringement”) of any person or entity. <br>
If you are a copyright owner, or authorized on behalf of one, and you believe that the copyrighted work has been copied in a way that constitutes copyright infringement, please submit your claim via legal email, with the subject line: “Copyright Infringement” and include in your claim a detailed description of the alleged Infringement as detailed below, under “DMCA Notice and Procedure for Copyright Infringement Claims” <br>
You may be held accountable for damages (including costs and attorneys’ fees) for misrepresentation or bad-faith claims on the infringement of any Content found on and/or through Service on your copyright. 

## DMCA Notice and Procedure for Copyright Infringement Claims 

You may submit a notification pursuant to the Digital Millennium Copyright Act (DMCA) by providing our Copyright Agent with the following information in writing (see 17 U.S.C 512(c)(3) for further detail):
- an electronic or physical signature of the person authorized to act on behalf of the owner of the copyright’s interest; 
- a description of the copyrighted work that you claim has been infringed, including the URL (i.e., web page address) of the location where the copyrighted work exists or a copy of the copyrighted work; 
- identification of the URL or other specific location on Service where the material that you claim is infringing is located; 
- your address, telephone number, and email address; 
- a statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law; 
- a statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the copyright owner or authorized to act on the copyright owner’s behalf.
You can contact our Copyright Agent via the legal email line. 

## Error Reporting and Feedback 

You may provide us either directly at [Feedback Category](https://discuss.higenku.org/c/site-feedback) with information and feedback concerning errors, suggestions for improvements, ideas, problems, complaints, and other matters related to our Service (“Feedback”). You acknowledge and agree that: 
- you shall not retain, acquire or assert any intellectual property right or other right, title or interest in or to the Feedback; 
- Project may have development ideas similar to the Feedback; 
- Feedback does not contain confidential information or proprietary information from you or any third party;
- Project is not under any obligation of confidentiality with respect to the Feedback. In the event the transfer of the ownership to the Feedback is not possible due to applicable mandatory laws, you grant Project and its affiliates an exclusive, transferable, irrevocable, free-of-charge, sub-licensable, unlimited and perpetual right to use (including copy, modify, create derivative works, publish, distribute and commercialize) Feedback in any manner and for any purpose. 

## Applications Distributed on the Store
Applications distributed in our Store have the copyright from their respective developers and publishers, by publishing applications in our store you grant us, free of charge for ilimited period, the right of distribution and installation.<br>
You may grant us the access to your source code when requested in case of our Project needing it, this includes but not limited to, Investigations conducted by our project, in case we find that your application is suspicious of abusing the user, the api or any related service. This investigations will start after an abuse report be reported to us, we will start by analysing the activity of your application on a sandbox environment and then if we confirm the report we may need to request access to your source code. In these cases your application will be suspended from our store and user with your app installed will be notified that your application is under investigations for abuse.<br>
We grant ourselves the right to remove, and notify user of abuse indentified in your app, the abuse in question is reported by our users, developers or ourselves.<br>
In Open source applications we the investigation may be conducted by a developer who doens't have any connection to us. In case of prioritary applications the investigations will be conducted by our developers.<br>
We grant the developers that all applications and libraries installed by our applications have the user consent and the user agreed to the license. The User therefore agree to respect the terms of the license and the warranty of policies of those, which may override these terms. In case of conflit please read the cases bellow:
   - if the application is trying to interact with one of the services provided by our application (including, but not limited to, apis, OAuth2.1, extensions system) and the user is inside of our domain (inside *.higenku.org, this maybe inside a consent screen in accounts.higenku.org for example), this terms of services shall be respected.
   - if the user is inside the application domain not making direct contact with our application (for example, myapp.co), then the terms and services of the application shall be respected.  <br>
External Applications available in [this subgroup on gitlab](https://gitlab.com/higenku/extapps/) which you can request more in [these repository](https://gitlab.com/higenku/extapps/higenku___requests). Are by no means from the Higenku Project, by installing them you agree that they aren't from us and that you will respect the intelectual copyright of the publisher. These applications are made available by us because some companies may not enter our platform because of the complexity involving their source code, they may not want to enter our platform by themselves, or they don't know our application, in these cases, we made their product available, by redestributing with the same license.

## Links To Other Web Sites 

Our Service may contain links to third party web sites or services that are not owned or controlled by Higenku Project. <br>
Higenku Project has no control over, and assumes no responsibility for the content, privacy policies, or practices of any third party web sites or services. We do not warrant the offerings of any of these entities/individuals or their websites. <br>
YOU ACKNOWLEDGE AND AGREE THAT PROJECT SHALL NOT BE RESPONSIBLE OR LIABLE, DIRECTLY OR INDIRECTLY, FOR ANY DAMAGE OR LOSS CAUSED OR ALLEGED TO BE CAUSED BY OR IN CONNECTION WITH USE OF OR RELIANCE ON ANY SUCH CONTENT, GOODS OR SERVICES AVAILABLE ON OR THROUGH ANY SUCH THIRD PARTY WEB SITES OR SERVICES. <br>
WE STRONGLY ADVISE YOU TO READ THE TERMS OF SERVICE AND PRIVACY POLICIES OF ANY THIRD PARTY WEB SITES OR SERVICES THAT YOU VISIT. 

## Disclaimer Of Warranty 

THESE SERVICES ARE PROVIDED BY PROJECT ON AN “AS IS” AND “AS AVAILABLE” BASIS. PROJECT MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, AS TO THE OPERATION OF THEIR SERVICES, OR THE INFORMATION, CONTENT OR MATERIALS INCLUDED THEREIN. YOU EXPRESSLY AGREE THAT YOUR USE OF THESE SERVICES, THEIR CONTENT, AND ANY SERVICES OR ITEMS OBTAINED FROM US IS AT YOUR SOLE RISK. <br>
NEITHER PROJECT NOR ANY PERSON ASSOCIATED WITH PROJECT MAKES ANY WARRANTY OR REPRESENTATION WITH RESPECT TO THE COMPLETENESS, SECURITY, RELIABILITY, QUALITY, ACCURACY, OR AVAILABILITY OF THE SERVICES. WITHOUT LIMITING THE FOREGOING, NEITHER PROJECT NOR ANYONE ASSOCIATED WITH PROJECT REPRESENTS OR WARRANTS THAT THE SERVICES, THEIR CONTENT, OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE SERVICES WILL BE ACCURATE, RELIABLE, ERROR-FREE, OR UNINTERRUPTED, THAT DEFECTS WILL BE CORRECTED, THAT THE SERVICES OR THE SERVER THAT MAKES IT AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS OR THAT THE SERVICES OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE SERVICES WILL OTHERWISE MEET YOUR NEEDS OR EXPECTATIONS. <br>
PROJECT HEREBY DISCLAIMS ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, STATUTORY, OR OTHERWISE, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT, AND FITNESS FOR PARTICULAR PURPOSE. <br>
THE FOREGOING DOES NOT AFFECT ANY WARRANTIES WHICH CANNOT BE EXCLUDED OR LIMITED UNDER APPLICABLE LAW. <br>
WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH 

## Limitation Of Liability 

EXCEPT AS PROHIBITED BY LAW, YOU WILL HOLD US HARMLESS FOR ANY INDIRECT, PUNITIVE, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGE, HOWEVER IT ARISES (INCLUDING ATTORNEYS’ FEES AND ALL RELATED COSTS AND EXPENSES OF LITIGATION AND ARBITRATION, OR AT TRIAL OR ON APPEAL, IF ANY, WHETHER OR NOT LITIGATION OR ARBITRATION IS INSTITUTED), WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE, OR OTHER TORTIOUS ACTION, OR ARISING OUT OF OR IN CONNECTION WITH THIS AGREEMENT, INCLUDING WITHOUT LIMITATION ANY CLAIM FOR PERSONAL INJURY OR PROPERTY DAMAGE, ARISING FROM THIS AGREEMENT AND ANY VIOLATION BY YOU OF ANY FEDERAL, STATE, OR LOCAL LAWS, STATUTES, RULES, OR REGULATIONS, EVEN IF PROJECT HAS BEEN PREVIOUSLY ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. EXCEPT AS PROHIBITED BY LAW, IF THERE IS LIABILITY FOUND ON THE PART OF PROJECT, IT WILL BE LIMITED TO THE AMOUNT PAID FOR THE PRODUCTS AND/OR SERVICES, AND UNDER NO CIRCUMSTANCES WILL THERE BE CONSEQUENTIAL OR PUNITIVE DAMAGES. SOME STATES DO NOT ALLOW THE EXCLUSION OR LIMITATION OF PUNITIVE, INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE PRIOR LIMITATION OR EXCLUSION MAY NOT APPLY TO YOU. 

## Termination 

We may terminate or suspend your account and bar access to Service immediately, without prior notice or liability, under our sole discretion, for any reason whatsoever and without limitation, including but not limited to a breach of Terms. <br>
If you wish to terminate your account, you may simply discontinue using Service. and delete your account in your preferences tab in [accounts.higenku.org](https://accounts.higenku.org/user/preferences)  <br>
All provisions of Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability. <br> 

## Governing Law 
These Terms shall be governed and construed in accordance with the laws of Brazil, which governing law applies to agreement without regard to its conflict of law provisions. <br>
Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service and supersede and replace any prior agreements we might have had between us regarding Service.

## Changes To Service 

We reserve the right to withdraw or amend our Service, and any service or material we provide via Service, in our sole discretion without notice. We will not be liable if for any reason all or any part of Service is unavailable at any time or for any period. From time to time, we may restrict access to some parts of Service, or the entire Service, to users, including registered users. 

## Amendments To Terms 

We may amend Terms at any time by posting the amended terms on this site. It is your responsibility to review these Terms periodically. <br>
Your continued use of the Service following the posting of revised Terms means that you accept and agree to the changes. You are expected to check this page frequently so you are aware of any changes, as they are binding on you. <br>
By continuing to access or use our Service after any revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, you are no longer authorized to use Service. 

## Waiver And Severability 

No waiver by Project of any term or condition set forth in Terms shall be deemed a further or continuing waiver of such term or condition or a waiver of any other term or condition, and any failure of Project to assert a right or provision under Terms shall not constitute a waiver of such right or provision. <br>
If any provision of Terms is held by a court or other tribunal of competent jurisdiction to be invalid, illegal or unenforceable for any reason, such provision shall be eliminated or limited to the minimum extent such that the remaining provisions of Terms will continue in full force and effect.

## Acknowledgement 

BY USING SERVICE OR OTHER SERVICES PROVIDED BY US, YOU ACKNOWLEDGE THAT YOU HAVE READ THESE TERMS OF SERVICE AND AGREE TO BE BOUND BY THEM. 

## Final
This document is a part of series of legal documents provided by our project.
You may also review the [Privacy Policy](/legal/pp) and the [Cookie Policy](/legal/cp)
